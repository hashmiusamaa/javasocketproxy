package edu.asupoly.ser421.socket;

import edu.asupoly.ser421.caching.CacheProvider;
import edu.asupoly.ser421.logging.Logger;
import edu.asupoly.ser421.parallel.Parallelizer;
import edu.asupoly.ser421.server.ServerDetails;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.*;

/**
 * @author hashmiusama
 */
public class SocketListener {
    private ServerSocket server;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();;
    public SocketListener() throws IOException {
        server = new ServerSocket(ServerDetails.localPort);
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Logger.log("Shutting down server.");
            Logger.fh.close();
            executor.shutdown();
            try {
                FileOutputStream fileOut =
                        new FileOutputStream(ServerDetails.diskCacheLocation);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                synchronized (CacheProvider.cache){
                    out.writeObject(CacheProvider.cache);
                }
                out.close();
                fileOut.close();
                System.out.printf("Serialized data is saved in cache.ser\n");
            }catch(IOException i) {
                i.printStackTrace();
            }
        }, "Shutdown-thread"));
    }
    public void listen() throws IOException {
        System.out.println(String.format("Your proxy to server %s:%d is initiated on port %d with cache size %d with delay %d with cache file %s and stale cache refresh time %d.", ServerDetails.serverAddress, ServerDetails.serverPort, ServerDetails.localPort, ServerDetails.cacheSize, ServerDetails.delay, ServerDetails.diskCacheLocation, ServerDetails.cacheStaleTime));
        while(true){
            Socket socket = null;
            try{
                 socket = server.accept();
            }catch(SocketTimeoutException e){
                server.close();
                System.out.println("Timing this server out because no connections.");
                System.exit(0);
            }
            if(socket != null) executor.submit(new Parallelizer(socket));
        }
    }
}
