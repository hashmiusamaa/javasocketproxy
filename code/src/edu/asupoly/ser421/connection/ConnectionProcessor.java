package edu.asupoly.ser421.connection;

import edu.asupoly.ser421.http.HTTPGetProcessor;
import edu.asupoly.ser421.logging.Logger;
import edu.asupoly.ser421.server.ServerDetails;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;


/**
 * @author hashmiusama
 */
public class ConnectionProcessor {
    public static void process(Socket socket){
        BufferedReader reader = null;
        PrintWriter writer = null;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.write("Welcome to the proxy server for " + ServerDetails.serverAddress + ":" + ServerDetails.serverPort);
            writer.write("\nPlease use 'GET <filename>' to get desired file.\n");
            writer.write(">>");
            writer.flush();
            String request = "";
            try{
                request = reader.readLine();
            }catch(SocketTimeoutException e){
                request = "";
            }catch (SocketException e){
                Logger.log("Socket gone.");
            }
            if(!request.equals("")){
                String[] input = request.split(" ");
                if(input.length == 2 && input[0].equalsIgnoreCase("get")){
                    Logger.log("I am sending back the input: " + request + "\n");
                    writer.write(new HTTPGetProcessor(request.split(" ")[1]).getFileFromServer() + "\n");
                } else {
                    writer.write("Invalid request " + request + "\n");
                }
                writer.write("end\n");
                writer.flush();
            }
            if(!socket.isClosed()) socket.close();
            Logger.log("Socket closed on " + Thread.currentThread().getName());
        } catch (IOException e) {
            Logger.log(e.getLocalizedMessage());
            Logger.log("Socket connection dropped.");
        }
    }
}
