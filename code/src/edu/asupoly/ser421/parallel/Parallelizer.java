package edu.asupoly.ser421.parallel;

import edu.asupoly.ser421.connection.ConnectionProcessor;
import edu.asupoly.ser421.logging.Logger;
import edu.asupoly.ser421.server.ServerDetails;

import java.net.Socket;

/**
 * @author hashmiusama
 */
public class Parallelizer implements Runnable {
    private Socket socket = null;
    public Parallelizer(Socket socket){
        this.socket = socket;
    }
    @Override
    public void run() {
        try {
            Thread.sleep(ServerDetails.delay);
        } catch (InterruptedException e) {
            Logger.log("Sleep Interrupted.");
        }
        ConnectionProcessor.process(socket);
    }
}
