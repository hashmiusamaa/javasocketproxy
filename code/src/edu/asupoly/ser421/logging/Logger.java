package edu.asupoly.ser421.logging;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;

/**
 * @author hashmiusama
 */
public class Logger {

    static java.util.logging.Logger LOGGER = null;
    public static Handler fh = null;
    static{
        LOGGER = java.util.logging.Logger.getAnonymousLogger();
        LOGGER.setLevel(Level.FINE);
        try {
            fh = new FileHandler("log.xml");
            LOGGER.addHandler(fh);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void log(String issue){LOGGER.fine(issue);}

}
