package edu.asupoly.ser421.caching;

import edu.asupoly.ser421.server.ServerDetails;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author hashmiusama
 */
class StaleCacheResolver extends Thread {
    private static List<String> keysInRetrievalOrderStaleTimeAgo = new ArrayList<>();
    private static final ScheduledExecutorService scheduler =
            Executors.newScheduledThreadPool(1);

    static void scheduleCacheSave() {
        scheduler.scheduleAtFixedRate(() -> {
            List<String> currentCacheKeysArray = new ArrayList<>(CacheProvider.cache.keySet());
            if(keysInRetrievalOrderStaleTimeAgo.size() > 0)
            {
                int i = 1;
                synchronized (CacheProvider.cache){
                    while(keysInRetrievalOrderStaleTimeAgo.get(keysInRetrievalOrderStaleTimeAgo.size()-i)
                            .equals(currentCacheKeysArray.get(currentCacheKeysArray.size()-i))){
                        if(keysInRetrievalOrderStaleTimeAgo.size()-i > 0) i++;
                        else break;
                    }
                    for(int j = i; j > 0; j--){
                        String s = currentCacheKeysArray.remove(currentCacheKeysArray.size()-j);
                        String value = CacheProvider.get(s);
                        CacheProvider.cache.setCurrentSize(CacheProvider.cache.getCurrentSize() - value.length());
                        CacheProvider.cache.remove(s);
                        }
                }
            }
            System.out.println("Stale Cache Removal Attempt.");
            keysInRetrievalOrderStaleTimeAgo = new ArrayList<>(CacheProvider.cache.keySet()) ;
        }, 10000, ServerDetails.cacheStaleTime, TimeUnit.MILLISECONDS);
    }
}
