package edu.asupoly.ser421.caching;

import edu.asupoly.ser421.logging.Logger;
import edu.asupoly.ser421.server.ServerDetails;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Singleton Cache
 * @author hashmiusama
 */
class LRUCache extends LinkedHashMap<String, String> {
    private static LRUCache cache = new LRUCache(ServerDetails.cacheSize);
    static{
        try {
            FileInputStream fileIn = new FileInputStream(ServerDetails.diskCacheLocation);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            cache = (LRUCache) in.readObject();
            System.out.println(cache);
            in.close();
            fileIn.close();
            StaleCacheResolver.scheduleCacheSave();
        }catch(IOException | ClassNotFoundException i) {
            Logger.log(i.getLocalizedMessage());
        }
    }
    private int maxSize;
    private int currentSize = 0;
    private LRUCache(int size){
        super(size, 0.75f, true);
        // Cache size in Kilobytes.
        this.maxSize = size*1024;
        Logger.log("Cache of maxSize " + this.maxSize + "bytes initialized.");
    }
    static LRUCache getCache(){
        return cache;
    }
    protected boolean removeEldestEntry(Map.Entry<String, String> eldest) {
        if (this.currentSize > this.maxSize){
            synchronized (cache){
                this.currentSize -= eldest.getValue().length();
            }
            return true;
        }else{
            return false;
        }
    }

    int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    int getCurrentSize() {
        return currentSize;
    }

    void setCurrentSize(int currentSize) {
        this.currentSize = currentSize;
    }

    @Override
    public String toString() {
        return cache.getCurrentSize() + " " + cache.keySet().toString();
    }
}
